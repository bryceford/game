package com.company.weaponry;

public class Dagger implements IWeapon {
    @Override
    public int additionalAttack() {
        return 0;
    }

    @Override
    public int additionalDefense() {
        return 0;
    }

    @Override
    public int additionalMagic() {
        return 0;
    }

    @Override
    public String name() {
        return "Dagger";
    }
}
