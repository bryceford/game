package com.company.weaponry;

public class Sword implements IWeapon{
    @Override
    public int additionalAttack() {
        return 100;
    }

    @Override
    public int additionalDefense() {
        return 0;
    }

    @Override
    public int additionalMagic() {
        return 0;
    }

    @Override
    public String name() {
        return "Sword";
    }
}
