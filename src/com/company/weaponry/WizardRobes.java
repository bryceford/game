package com.company.weaponry;

public class WizardRobes implements IWeapon {


    @Override
    public int additionalAttack() {
        return 0;
    }

    @Override
    public int additionalDefense() {
        return 0;
    }

    @Override
    public int additionalMagic() {
        return 100;
    }

    @Override
    public String name() {
        return "Robes of the Lizard Wizard";
    }
}
