package com.company.weaponry;

public interface IWeapon {

    int additionalAttack();

    int additionalDefense();

    int additionalMagic();

    String name();
}
