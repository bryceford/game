package com.company.weaponry;

public class Helmet implements IWeapon {

    @Override
    public int additionalAttack() {
        return 0;
    }

    @Override
    public int additionalDefense() {
        return 100;
    }

    @Override
    public int additionalMagic() {
        return 0;
    }

    @Override
    public String name() {
        return "Helmet";
    }
}
