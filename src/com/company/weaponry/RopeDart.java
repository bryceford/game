package com.company.weaponry;

public class RopeDart implements IWeapon {
    @Override
    public int additionalAttack() {
        return 300;
    }

    @Override
    public int additionalDefense() {
        return 100;
    }

    @Override
    public int additionalMagic() {
        return 200;
    }

    @Override
    public String name() {
        return "Rope Dart";
    }
}
