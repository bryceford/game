package com.company;

import com.company.characters.CharacterBase;

public interface ICharacter {

    int getHealthP();

    void attack(CharacterBase oponent);

    void defend(int attack);

    boolean isAlive();
}
