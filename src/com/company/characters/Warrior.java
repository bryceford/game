package com.company.characters;

public class Warrior extends CharacterBase{

    public Warrior(String name) {
        super(name, 100, 500, 400);
    }

    @Override
    public String getTitle() {
        return name + " the Warrior";
    }
}
