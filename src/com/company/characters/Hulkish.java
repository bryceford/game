package com.company.characters;

public class Hulkish extends CharacterBase {
    public Hulkish(String name) { super(name, 450, 450, 100); }

    @Override
    public String getTitle() {
        return name + " Hulk Smash";
    }
}
