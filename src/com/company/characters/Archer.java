package com.company.characters;

public class Archer extends  CharacterBase{

    public Archer(String name) {
        super(name, 300, 300, 400);
    }

    @Override
    public String getTitle() {
        return name + " the Archer";
    }
}
