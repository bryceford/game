package com.company.characters;

public class Rogue extends CharacterBase{
    public Rogue(String name) {
        super(name, 400, 400, 100);
    }

    public String getTitle() {
        return name + " the rogue";
    }
}
