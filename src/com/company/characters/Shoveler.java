package com.company.characters;

import com.company.characters.CharacterBase;

public class Shoveler extends CharacterBase {
    public Shoveler(String name){
        super (name, 400, 400, 200 );
    }

    public String getTitle(){
        return name + " The Shoveler";
    }
}
