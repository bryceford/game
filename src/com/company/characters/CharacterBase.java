package com.company.characters;

import com.company.weaponry.IWeapon;

import java.util.ArrayList;
import java.util.List;

public abstract class CharacterBase {

    private final int maxTotalPoints = 1000;
    private final int healthP = 1000;
    private int damageP = 0;
    private final int magicP;
    private final int attackP;
    private final int defenseP;
    private final List<IWeapon> inventory;
    public final String name;

    public CharacterBase(String name, int attack, int defense, int magic) {
        this.name = name;
        attackP = attack;
        defenseP = defense;
        magicP = magic;
        inventory = new ArrayList<>();
        int totalPoints = magicP + attackP + defenseP;
        if (totalPoints > maxTotalPoints) throw new NumberFormatException("too many points on character");
    }

    public int getHealthP() {
        return healthP - damageP;
    }

    public int getDamageP() {
        return damageP;
    }

    private void setDamageP(int damageP) {
        if (damageP > 0)
            this.damageP += damageP;
    }

    public void addWeapon(IWeapon weapon) {
        inventory.add(weapon);
    }

    public List<IWeapon> getWeapons() {
        return inventory;
    }

    public int getMagicP() {
        int inventoryTotal = 0;
        for (IWeapon weapon : inventory) {
            inventoryTotal += weapon.additionalMagic();
        }
        return magicP + inventoryTotal;
    }

    public int getAttackP() {
        int inventoryTotal = 0;
        for (IWeapon weapon : inventory) {
            inventoryTotal += weapon.additionalAttack();
        }
        return attackP + inventoryTotal;
    }

    public int getDefenseP() {
        int inventoryTotal = 0;
        for (IWeapon weapon : inventory) {
            inventoryTotal += weapon.additionalDefense();
        }
        return defenseP + inventoryTotal;
    }

    public boolean isAlive(){
       return damageP < healthP;
    }

    public void attack(CharacterBase openent) {
        openent.defend(getAttackP() + (getMagicP()/2));
    }

    public abstract String getTitle();

    private void defend(int attack) {
        setDamageP(attack - (getDefenseP() + (getMagicP()/2)));
    }
}
