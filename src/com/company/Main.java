package com.company;

import com.company.characters.*;
import com.company.weaponry.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static ArrayList<CharacterBase>players = new ArrayList<>();
    static ArrayList<IWeapon>weapons = new ArrayList<>();
    static ArrayList<CharacterBase> arenaPlayers = new ArrayList<>();
    static String message = "";
    static String menuTitle;
    static Arena arena = new Arena();

    static final String[] mainMenu = {"(1) List Characters", "(2) Enter Arena"};
    static final String[] characterMenu = {"(1) Add Weapon", "(2) Send To Arena"};

    public static void main(String[] args) {
        players.add(new Rogue("Eugor"));
        players.add(new YeetimusMaximus("Yeem"));
        players.add(new Shoveler("Brick"));
        players.add(new Archer("Robin H."));
        players.add(new Hulkish("Bruce"));
        players.add(new Warrior("Fred"));
        weapons.add(new CrossBow());
        weapons.add(new Shovel());
        weapons.add(new Sword());
        weapons.add(new Dagger());
        weapons.add(new NerfGun());
        weapons.add(new WizardRobes());
        weapons.add(new RopeDart());
        weapons.add(new Helmet());
	    while (mainMenu());
    }

    public static String getUserInput() {
        String input = null;
        try {
            input = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return input;
    }

    public static void output(String msg) {
        System.out.println("---> " + msg);
    }

    public static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static boolean mainMenu() {
        menuTitle = "Main Menu";
        final int selectedOption = showMenu(mainMenu);
        boolean result = true;

        switch (selectedOption) {
            case 0:
                result = false;
                break;
            case 1:
                while (selectPlayer());
                break;
            case 2:
                while (arenaMenu());
                break;
            default:
                message ="invalid choice try again";
                break;
        }
        return result;
    }

    public static boolean selectPlayer() {
        menuTitle = "list of characters";
        final CharacterBase character = getPlayerMenu(players);
        if (character == null) return false;
        while (characterMenu(character));
        return true;
    }

    public static CharacterBase getPlayerMenu(ArrayList<CharacterBase> playerList) {
        String[] playerMenu = new String[playerList.size()];
        for (int i = 0; i < playerList.size(); ++i) {
            playerMenu[i] = "(" + (i + 1) + ") " + playerList.get(i).name;
        }

        final int option = showMenu(playerMenu);
        return option > 0 ? playerList.get(option - 1) : null;
    }

    public static boolean characterMenu(CharacterBase player) {
        boolean resume = true;
        menuTitle = "Character Menu";

        message = showStats(player);

        int selection = showMenu(characterMenu);
        switch (selection) {
            case 1:
                final IWeapon iweap = weaponMenu();
                if (iweap != null) {
                    player.addWeapon(iweap);
                    message = "weapon added";
                } else {
                    message = "invalid weapon";
                }
                break;
            case 2:
                arenaPlayers.add(player);
                message = "Added to the Arena";
                break;
            case 0:
                resume = false;
                menuTitle = "main menu";
                message = "";
                break;
            default:
                message = "Invalid Selection";
        }
        return resume;
    }

    public static String showStats(CharacterBase player){
        String inventory = "";
        for (IWeapon weapon : player.getWeapons()) {
            inventory += weapon.name();
        }
        return player.name + ": " + "Player stats\nAlive:\t" + player.isAlive() +
                progressBar("\nHealth", player.getHealthP())+
                progressBar("\nAttack", player.getAttackP())+
                progressBar("\nDefense", player.getDefenseP())+
                progressBar("\nMagic", player.getMagicP())+
                "\ninventory\t"+ inventory;
    }

    public static IWeapon weaponMenu(){
        return null;
    }

    public static boolean arenaMenu() {
        menuTitle = "in arena";
        final int option = showMenu("(1) Select Player 1", "(2) Select Player 2", "(3) Begin Player Battle", "(4) Player 1 stats", "(5) Player 2 stats");
        switch (option) {
            case 0:
                return false;
            case 1:
                menuTitle = "Player 1";
                arena.setPlayer1(getPlayerMenu(arenaPlayers));
                break;
            case 2:
                menuTitle = "Player 2";
                arena.setPlayer2(getPlayerMenu(arenaPlayers));
                break;
            case 3:
                arena.battle(5);
                break;
            case 4:
                message = showStats(arena.getPlayer1());
                break;
            case 5:
                message = showStats(arena.getPlayer2());
                break;
            default:
                message = "Invalid Selection";
                break;
        }
        return true;
    }

    public static String progressBar(String type, int number) {
        int size = 100;

        StringBuilder text = new StringBuilder(type);
        text.append("             ");
        StringBuilder bar = new StringBuilder(text.substring(0,10));
        bar.append(number).append("\t").append("|");
        for (int i = 0; i < number/10; ++i) {
            --size;
            bar.append("#");
        }
        for (int j = 0; j < size; ++j) {
            bar.append("-");
        }
        bar.append("|");
        return bar.toString();
    }

    public static int showMenu(String... options){
        clearScreen();
        output(menuTitle +"\n" + message + "\n");
        message = menuTitle = "";
        output("select an option:");
        for (String option : options) {
            output(option);
        }
        output("(0) exit menu");
        final String selection = getUserInput().toLowerCase();
        int value = 0;
        try {
            value = Integer.parseInt(selection);
        } catch (NumberFormatException e) {
            output("Please Select A Number");
        }
        return value;
    }
}
