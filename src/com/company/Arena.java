package com.company;

import com.company.characters.CharacterBase;

import java.util.Random;

public class Arena {

    private CharacterBase player1;
    private CharacterBase player2;
    private final Random random = new Random();


    public CharacterBase getPlayer2() {
        return player2;
    }

    public void setPlayer2(CharacterBase player2) {
        this.player2 = player2;
    }

    public CharacterBase getPlayer1() {
        return player1;
    }

    public void setPlayer1(CharacterBase player1) {
        this.player1 = player1;
    }

    public void battle(int rounds) {
        for (int i = 0; i < rounds; ++i) {
            performBattle();
        }
    }

    private void performBattle() {
        if (random.nextBoolean()) {
            player1.attack(player2);
        } else {
            player2.attack(player1);
        }
    }
}
